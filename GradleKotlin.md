
# Wie erstelle ich ein Kotlin-Projekt mit Gradle?

Legen Sie in einem Verzeichnis Ihrer Wahl einen Ordner an mit einem Namen, der das Projekt treffend beschreibt. (Im Beispiel "tictactoe" genannt) Mit dem Windows-Kommando `mkdir` (_make directory_) erstellt man einen Ordner (man spricht auch von einem Verzeichnis), und mit dem Kommando `cd` (_change directory_) wechselt man in das angegebene Verzeichnis.

~~~ 
C:\Users\Jonas\Desktop\Pis2020>mkdir tictactoe

C:\Users\Jonas\Desktop\Pis2020>cd tictactoe

C:\Users\Jonas\Desktop\Pis2020\tictactoe>gradle init
~~~

Gradle hilft einem bei der Initialisierung eines Projektverzeichnisses.    
Auch wenn Wir die Javalin Applikation mit Kotlin erstellen wollen, so wollen Wir dennoch für unsere `build.gradle` Groovy statt Kotlin benutzen.   

~~~ shell
Select type of project to generate:
  1: basic
  2: application
  3: library
  4: Gradle plugin
Enter selection (default: basic) [1..4] 2

Select implementation language:
  1: C++
  2: Groovy
  3: Java
  4: Kotlin
  5: Swift
Enter selection (default: Java) [1..5] 4

Select build script DSL:
  1: Groovy
  2: Kotlin
Enter selection (default: Kotlin) [1..2] 1

Project name (default: tictactoe):
Source package (default: tictactoe):

BUILD SUCCESSFUL in 8s
2 actionable tasks: 2 executed
~~~

Gradle hat einige Dateien angelegt. Wichtig für Sie ist vor allem die angelegte Ordnerstruktur für Ihre Quelldateien. Sie sehen, dass Gradle zwei Beispieldateien angelegt hat. (Das Kommando `dir` listet die Inhalte eines Verzeichnisses, _directory_, auf. Die Option `/s` zeigt auch den Inhalt der Unterverzeichnisse an, `/b` stellt die Auflistung kompakt zusammen.)

~~~ shell
C:\Users\Jonas\Desktop\Pis2020\tictactoe>dir /s /b

C:\Users\Jonas\Desktop\Pis2020\tictactoe\.gitattributes
C:\Users\Jonas\Desktop\Pis2020\tictactoe\.gitignore
C:\Users\Jonas\Desktop\Pis2020\tictactoe\.gradle
C:\Users\Jonas\Desktop\Pis2020\tictactoe\build.gradle
C:\Users\Jonas\Desktop\Pis2020\tictactoe\gradle
C:\Users\Jonas\Desktop\Pis2020\tictactoe\gradlew
C:\Users\Jonas\Desktop\Pis2020\tictactoe\gradlew.bat
C:\Users\Jonas\Desktop\Pis2020\tictactoe\settings.gradle
C:\Users\Jonas\Desktop\Pis2020\tictactoe\srcC:\Users\Jonas\Desktop\Pis2020\tictactoe\src\main
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\test
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\main\kotlin
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\main\resources
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\main\kotlin\tictactoe
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\main\kotlin\tictactoe\App.kt
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\test\kotlin
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\test\resources
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\test\kotlin\tictactoe
C:\Users\Jonas\Desktop\Pis2020\tictactoe\src\test\kotlin\tictactoe\AppTest.kt
~~~
> Viele dieser Dateien werden sie gar nicht brauchen.
> Wenn Sie mit Git arbeiten, denken Sie daran, eine .gitignore-Datei anzulegen, falls Gradle dies nicht automatisch für sie erledigt hat.
> All die automatisch und temporär angelegten Dateien, die Gradle erzeugt (wie z.B. die kompilierten Kotlin-Dateien), haben nichts in einem Git-Repository verloren. Git dient hauptsächlich zur Verwaltung des Quellcodes in `/src`!
> Sie können dann in der .gitignore Datein wie den .gradle Ordner dann dort einfügen, sodass diese von git ignoriert werden. Siehe [docs/gitignore](https://git-scm.com/docs/gitignore)

Wenn Sie mögen, können Sie die Beispieldatei kompilieren und ausführen lassen.

~~~ 
C:\Users\Jonas\Desktop\Pis2020\tictactoe>gradle run

> Task :run
Hello world.

BUILD SUCCESSFUL in 1s
2 actionable tasks: 2 executed
~~~




