# Der kleine Gauß: Von 1 bis 100

Der kleine [Carl Friedrich Gauß](https://de.wikipedia.org/wiki/Carl_Friedrich_Gau%C3%9F) bekam in der Schule von seinem Lehrer eine einfache Aufgabe gestellt: "Berechne mir die Summe der Zahlen von 1 bis 100!" Vermutlich erhoffte sich der Lehrer, Carl Friedrich für eine Weile ruhig zu stellen mit der mühsamen schriftlichen Addition der Zahlen. Der aber gab nach kurzer Zeit das Ergebnis bekannt: 5050. (Der [Rechenkniff](https://de.wikipedia.org/wiki/Carl_Friedrich_Gau%C3%9F#Eltern,_Kindheit_und_Jugend) ist auf Wikipedia nachzulesen.)

## Gauß deklarativ programmiert

Wie hätten Sie die Aufgabe in Java umgesetzt? Mit einer Schleife? Es geht anders!

~~~
jshell> IntStream.rangeClosed(1,100).sum()
$1 ==> 5050
~~~

Das liest sich nicht mehr wie ein Programm, sondern wie eine Beschreibung der Aufgabe. Man nähert sich mit modernem Java einem deklarativen (beschreibenden) Programmierstil an!

Das `sum()` ist tatsächlich durch ein `reduce` realisiert. So geht es also auch:

~~~
jshell> IntStream.rangeClosed(1,100).reduce(0, (x,y) -> x + y)
$2 ==> 5050
~~~

Da es die Methode `sum` für die Addition zweier `Integer` gibt, geht es auch elegant mit einer Methodenreferenz:

~~~
jshell> IntStream.rangeClosed(1,100).reduce(0, Integer::sum)
$3 ==> 5050
~~~

Möchte man sich den unterwegs erzeugten Strom aus Ganzzahlen anschauen, kann man ein `peek` einfügen mit einem Seiteneffekt als Ausgabe. Einer kürzeren Ausgabe zuliebe berechnen wir jetzt nur die Summe von 1 bis 10.

~~~
jshell> IntStream.rangeClosed(1,10).peek(System.out::println).sum()
1
2
3
4
5
6
7
8
9
10
$4 ==> 55
~~~

## Sind Streams nicht langsamer als `for`-Schleifen?

Performanz-Messungen sind eine komplizierte Sache, nicht nur in Java. Aber um ein Gefühl für die Unterschiede in der Ausführung zu bekommen, vergleichen wir die Laufzeiten der folgenden Arten, die Summe der Zahlen von 1 bis 1 Milliarde zu bestimmen.

1. Variante: Ein Strom aus Zahlen
   
    ```
    LongStream.rangeClosed(1,1_000_000_000).sum();
    ```

2. Variante: Ein paralleler Strom aus Zahlen

    ```
    LongStream.rangeClosed(1,1_000_000_000).parallel().sum();
    ```

3. Variante: Eine `for`-Schleife

    ```
    long sum = 0;
    for(long x = 1; x <= 1_000_000_000; x++) sum += x;
    ```

Ich habe diese Code-Fragmente mit dem folgenden Stückchen Code mit Hilfe der JShell ausgeführt und vermessen. Die Ausgabe ist in Millisekunden.

~~~ Java
int N = 40;
long start, stopp;
long add = 0;
for(int i = 1; i <= N; i++) {
    start = System.nanoTime();
    // insert code snippet here
    stopp = System.nanoTime();
    add += stopp - start;
}
System.out.println(add/N/1_000_000L);
~~~

Auf meinem Laptop (Intel i7-5500U, 2.40GHz, 2 Kerne) ergeben sich folgende Laufzeiten:

| Variante | 1.    | 2.    | 3.    |
| -------- | ----- | ----- | ----- |
|      ms  | 682   | 317   | 468   |

Der nicht-parallelisierte Stream ist um 45% langsamer als die `for`-Schleife. Das ist ein bezahlbarer Preis für deklarativen Code. Allerdings schenkt einem `parallel()` bei zwei Kernen einen doppelt so schnellen Code, was beeindruckend ist. Jetzt erledigt der Strom die Arbeit um 32% schneller als die `for`-Schleife.

> Fazit: Unterschätze die Performanz eines deklarativen Programmierstils nicht, vor allem `parallel()` kann sich als echter Durchstarter erweisen.


